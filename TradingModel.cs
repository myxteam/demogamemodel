﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TreasureGameModels
{
    public class TradingModel
    {
        public string OfferCurrency { get; set; }
        public string BoughtCurrency { get; set; }
        public int OfferAmount { get; set; }
        public int BoughtAmount { get; set; }
    }
}
