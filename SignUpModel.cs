using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TreasureGameModels
{
    [Serializable]
    public class SignUpModel
    {

        public DateTime BirthDay;
        public String UserName;
        public String Password;
    }
}
