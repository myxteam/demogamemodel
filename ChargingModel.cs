using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace TreasureGameModels
{
    [Serializable]
    public class ChargingModel
    {
        int ChargingID;
        int ChargeAmmount;
        String CurrencyName;
        List<ChargingModelEntry> Currencies;
    }

    [Serializable]
    public class ChargingModelEntry
    {
        int Ammount;
        String CurrencyName;
    }
}