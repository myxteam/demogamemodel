﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TreasureGameModels
{
    [Serializable]
    public class GamePackageModel
    {
        public GamePackageModel() { 
                
        }

        public string Name { get; set; }
        public GameCurrencies GameCurrency { get; set; }
        public int PackageValue { get; set; }
        public int PackagePrice { get; set; }

    }
}
