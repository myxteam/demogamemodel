﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TreasureGameModels
{
    public class GameCurrenciesConstant
    {
        //Chest Grid Constant
        public const string GOLD = "GOLD";
        public const string SILVER = "SILVER";
		public const string COPPER = "COPPER";
        public const string DOUBLOON = "DOUBLOON";
        public const string ANKH = "ANKH"; 

		public const int GOLD_ID = 1;
		public const int SILVER_ID = 2;
		public const int COPPER_ID = 3;
		public const int DOUBLOON_ID = 4;
        public const int ANKH_ID = 4;

    }

    public enum GameCurrencies { 
        GOLD = 1,
        SILVER = 2,
        COPPER = 3,
        DOUBLOON = 4
    }
}
