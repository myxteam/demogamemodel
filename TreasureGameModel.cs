using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace TreasureGameModels
{
    [Serializable]
    public class TreasureGameModel
    {
        public int Rows;
        public int Collums;
        public String SetupString;
        public int SessionID;
    }
    [Serializable]
    public class TransactionModel
    {
        public DateTime Date;
        public String Transaction;
        public String Info;
        public Guid GameID;
        public TransactionDetailsModel TransactionDetail;
    }

    [Serializable]
    public class TransactionDetailsModel
    {
        public int Id;
        public string Data1;
        public string Data2;
        public string Data3;
    }
}