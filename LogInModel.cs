using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace TreasureGameModels
{
    [Serializable]
    public class LogInModel
    {
        public String UserName;
        public String Password;
        public String CreelUserName;
        public String MacAddress;
        public String DeviceName;
        public String Manufacturer;
        public double Amount;
        public int tries;
        public String Model;
		public Guid UserGuid;
        public Guid CreelGuid;
        public Guid GameID;
        public int DeviceInch;
        public string DeviceId;
    }
}