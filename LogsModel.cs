﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TreasureGameModels
{
    public class LogsModel
    {
        public string Message { get; set; }
        public DateTime date { get; set; }
    }
    public class TransactionLogsModel
    {
        public Guid UserGuid { get; set; }
        public string MacID { get; set; }
        public List<LogsModel> logModels { get; set; }
    }
}
