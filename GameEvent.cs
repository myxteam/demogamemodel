using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace TreasureGameModels
{
    [Serializable]
   public class GameEvent
    {
        public int CurrencyID;
        public int Amount;
        public int Cost;
        public int UserId;
        public int SessionID;

        public String Username;
        public String Password;
        public Guid UserGuid;
        public Guid GameId;
    }
}