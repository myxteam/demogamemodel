using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TreasureGameModels
{
[Serializable]
   public class ApiResult
    {
       public ApiResponse Status;
       public String ErrorString;
       public String ErrorCode;
       public Object Data;
       public string infoString;
    }

[Serializable]
    public enum ApiResponse
    {
        NETWORK_ERROR = 1 ,
        OTHER_ERROR= 2,
        CREEL_ERROR = 3,
        OK = 4,
        CREEL_PENDING = 5,
        CREEL_DEAUTHORIZE = 6,
        CREEL_PENDING_DEAUTHORIZE = 7
    }
}   