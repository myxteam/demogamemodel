﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TreasureGameModels
{
    public class AuthenticatedTradingModel
    {
        public String MacAddress;
        public Guid UserGuid;
        public Guid GameID;
        public TradingModel tm;
    }
}
