﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Creel.Common.Data
{
    public class CustomerAccount
    {
        Guid customerAccountId;
        string displayName;

        public Guid CustomerAccountId
        {
            get { return customerAccountId; }
            set { customerAccountId = value; } 
        } 

        public string DisplayName
        {
            get { return displayName; }
            set { displayName = value; }
        }
    }
}
