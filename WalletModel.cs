using System;
using System.Collections.Generic;

namespace TreasureGameModels
{
    [Serializable]
    public class WalletModel
    {
        public String Username;
        public String Password;
        public Guid Guid;

        public int ChargingID;
        public int ChargeAmmount;
        public String CurrencyName;
        public List<WalletModelEntry> Currencies;

        public int DIRECT;
    }

    [Serializable]
    public class WalletModelEntry
    {
        public int BalanceID;
        public String CurrencyName;
        public double Amount;
    }


    [Serializable]
    public class TransferToWalletModel
    {
        public LogInModel LoginModel;
        public int Ammount;
        public Guid GameID;
    }

    [Serializable]
    public class TransferToGameModel
    {
        public LogInModel LoginModel;
        public int Ammount;
        public Guid GameID;
    }

    [Serializable]
    public class TransferModel
    {
        public Guid UserGuid;
        public Guid CreelGuid;
        public Decimal Amount;
        public Guid GameID;
        public string DeviceID;
    }

    [Serializable]
    public class TransferToUserModel
    {
        public string FROM_USERNAME;
        public Guid USER_GUID;
        public Guid FROM_CREEL_GUID;
        public string TO_USERNAME;
        public Guid TO_CREEL_GUID;
        public int Amount;
        public Guid GameID;
        public string DeviceID;
        public string Message;
    }
}

