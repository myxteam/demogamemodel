﻿using System;

namespace TreasureGameModels
{
	public class NotificationHubRegistration
	{
		public string Platform { get; set; }
        public string UserName { get; set; }
        public bool NotifyOnRegister { get; set; }                        
        public string DeviceId { get; set; }  
	}
}

