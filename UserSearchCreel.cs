﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TreasureGameModels
{
    public class UserSearchCreel
    {
        private Guid currentCustomerAccountId;
        private string deviceId;
        private string searchKeyword;

        public string DeviceId
        {
            get { return deviceId; }
            set { deviceId = value; }
        }

     

        public Guid CurrentCustomerAccountId
        {
            get { return currentCustomerAccountId; }
            set { currentCustomerAccountId = value; }
        }

        public string SearchKeyword
        {
            get { return searchKeyword; }
            set { searchKeyword = value; }
        }
    }
}
